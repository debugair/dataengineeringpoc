##################################################################################################################
#
#   AUTHOR:     debugair (me@fullbox.ch)
#   DATE:       08/05/2020
#   PURPOSE:    Playground for a Data Engineer PoC
#
##################################################################################################################
#
#   DOCKER IMAGE BASED ON UBUNTU 20.10 WITH THE FOLLOWING COMPONENTS
#       | COMP              | VER                 | PORT        |
#       |--------------------------------------------------------
#       | PYTHON            | 2 & 3               |             |
#       | AIRFLOW           |                     | 8082        |
#       | SPARK             | 2.4.5               | 8088        |
#       | HADOOP            | 3.2.1               | 9870, 9000  |
#       | ZEPPELIN          | 0.9.0-preview1      | 8081        |
#
#   TO START THE CONTAINER WITH ALL THE NEEDED PORTS EXPOSED USE:
#       docker run --name poc -p 2021:22 -p 8081:8081 -p 8082:8082 -p 8042:8042 -p 8088:8088 -p 9000:9000 -p 9870:9870 dataengineerpoc
#
#   OR INTERACTIVE IN BASH
#       docker run -it --name poc -p 2021:22 -p 8081:8081 -p 8082:8082 -p 8042:8042 -p 8088:8088 -p 9000:9000 -p 9870:9870 dataengineerpoc bash
#
##################################################################################################################
ARG IMG=ubuntu:18.04

FROM $IMG
LABEL MAINTAINED "debugair"
USER root

COPY . /docker_build
RUN /docker_build/scripts/01_prepare.sh
RUN /docker_build/scripts/02_configure.sh

ENTRYPOINT ["/etc/bootstrap.sh", "-d"]

EXPOSE 22 8042 8082 8081 8088 9870 9000
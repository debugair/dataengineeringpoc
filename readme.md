# Data Engineering PoC


## Starting point
With this proof of concept (PoC) we want to implement a common Data Engineer task. The use case to implement contains the following steps:
* check an internet-based data source for files
* check if there are files available that do not reside on our system
* download the missing files to our system
* move them to a persistent layer before to process them
* process the files (for analytical purposes) put the results on a persistent layer
* visualize the results

Further we want those things to happen automatically means, that the execution of the steps is triggered by an entity that has scheduling capabilities.

> **_IMPORTANT NOTE:_**  The written code has PoC quality and is far from productional code. The goal was to have a quick and running solution that chains the used tools together to demonstrate how they can be orchestrated in a typical data engineering / processing use case.


## Tools
For this PoC we did pick the following tools to make the use case fly:
| TOOL      | VERSION           | PURPOSE                             |
|-----------|-------------------|-------------------------------------|
| docker    |  x                | to bundle & orchestrate all tools   |
| python    |  3                | to code all the logic needed        |
| hadoop    |  3.2.1            | to persist data                     |
| spark     |  2.4.5            | to process data                     |
| airflow   |   1.10.10         | as a trigger and schedule engine    |
| zeppelin  |  0.9.0-preview1   | to visualize results                |


## Docker container
The */Dockerfile* has been kept very lean and most of the setup logic has been moved to *.sh* files. The docker image uses a */scripts/bootstrap.sh* script that runs when a container is started. Within this script all the needed services are started and the system is prepared to be used. The docker container exposes the following ports:
| PORT  | PURPOSE      | URL                    |
|-------|--------------|------------------------|
| 8081  | zeppelin ui  | http://localhost:8081  |
| 8082  | airflow ui   | http://localhost:8082  |
| 8088  | yarn         | http://localhost:8088  |
| 9000  | hdfs         | x                      |
| 9870  | hadoop fs ui | http://localhost:9870  |


## Build container
To build the docker image use the following command:
```
docker build --pull --rm -f "Dockerfile" -t dataengineerpoc:latest "Docker"
```


## Start container
The container can be started with the following command:
```
docker run -it --name poc -p 2021:22 -p 8082:8082 -p 8081:8081 -p 8088:8088 -p 9000:9000 -p 9870:9870 dataengineerpoc bash
```
With this command the docker container will be started into bash so that the container can directly be accessed in bash to use it for further commands.


## Airflow GleifLeiDag
When the container is started airflow will trigger the DAG */airflow/dags/airflowGleifLeiDag.py* what triggers a BashOperator that executes */airflow/dags/src/python/gleifLeiDownload.py*. The code executed checks the internet (https://www.gleif.org/en/lei-data/gleif-golden-copy/download-the-golden-copy#/) for new GLEIF LEI data. It checks the available online files (the max lookup history can be controlled by a parameter) and checks hadoop for missing files. If there are new files they will be downloaded to the local system */opt/download/*. The zip archives will be unpacked and uploaded to hadoop */data/Lei/Raw/*. The DAG has a schedule that will be triggered every 5 minutes. To check if the DAG is running, open the url: *http://localhost:8082/admin/airflow/tree?dag_id=downloadGleifLeiData*. To check the progress if the DAG is running either use the following command in the shell of the container:
```
hdfs dfs -ls /data/Lei/Raw
```
or open the url: *http://localhost:9870/explorer.html#/data/Lei/Raw*


## Airflow HdfsDumpDag
When the container is started airflow will trigger another DAG */airflow/dags/airflowHdfsUsage.py* what triggers the BashOperation to trigger the script */airflow/dags/src/python/dumpHdfsUsage.py*. The code executes a hdfs file system dump (folders / files & sizes). Those dumps are uploaded to hadoop */data/Hdfs/Dumps/*. The DAG has a schedule that will be triggered every minute. o check if the DAG is running, open the url: *http://localhost:8082/admin/airflow/tree?dag_id=dumpHdfsStatus*. To check the progress if the DAG is running either use the following command in the shell of the container:
```
hdfs dfs -ls /data/Hdfs/Dumps
```
or open the url: *http://localhost:9870/explorer.html#/data/Hdfs/Dumps*

## Screenshots
![Airflow overview of all DAGs](https://gitlab.com/debugair/dataengineeringpoc/-/tree/develop/images/Airflow_DAG_Overview.png)
![Airflow detail view of a DAG](https://gitlab.com/debugair/dataengineeringpoc/-/blob/develop/images/Airflow_DAG_Detail.png)
![Zeppelin visualization of HDFS Dumps](https://gitlab.com/debugair/dataengineeringpoc/-/tree/develop/images/Zeppelin_HDFS_Data.png)
![Zeppelin visualization of GleifLei CH Data](https://gitlab.com/debugair/dataengineeringpoc/-/tree/develop/images/Zeppelin_GleifLei_Data.png)

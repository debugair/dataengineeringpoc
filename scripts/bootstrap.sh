#!/bin/bash

echo "START bootstrap.sh ..."
##########################################################################
#   ENV VARS
##########################################################################
${HADOOP_HOME:=/opt/hadoop}
: ${SPARK_PREFIX:=/opt/spark}
: ${SPARK_HOME:=/opt/spark}
: ${ZEPPELIN_HOME:=/opt/zeppelin}
: ${AIRFLOW_HOME:=/opt/zeppelin}
export HADOOP_HOME="/opt/hadoop"
export SPARK_HOME="/opt/spark"
export SPARK_PREFIX="/opt/spark"
export ZEPPELIN_HOME="/opt/zeppelin"
export AIRFLOW_HOME="/opt/airflow"
$HADOOP_HOME/etc/hadoop/hadoop-env.sh
$SPARK_PREFIX/conf/spark-env.sh
$ZEPPELIN_HOME/conf/zeppelin-env.sh


##########################################################################
#   SET HOSTNAME IN core-site.xml
##########################################################################
sed s/HOSTNAME/$HOSTNAME/ /opt/hadoop/etc/hadoop/core-site.xml.template > /opt/hadoop/etc/hadoop/core-site.xml


##########################################################################
#   START HDFS AND WAIT FOR IT TO RUN
##########################################################################
echo "START HDFS / YARN"
service ssh start
nohup $HADOOP_HOME/sbin/start-all.sh &>/dev/null &
while ! nc -z $HOSTNAME 9000; do
  sleep 1 # wait for 1 second before check again
done
echo "WAIT FOR HDFS"


##########################################################################
#   START ZEPPELIN
##########################################################################
$ZEPPELIN_HOME/bin/zeppelin-daemon.sh start &>/dev/null &


##########################################################################
#   START AIRFLOW
##########################################################################
nohup /usr/local/bin/airflow scheduler &>/dev/null &
nohup /usr/local/bin/airflow webserver -p 8082 --pid /opt/airflow/webserver.pid &>/dev/null &
nohup /usr/local/bin/airflow scheduler &>/dev/null &


/opt/spark/bin/spark-submit /opt/airflow/dags/src/pyspark/kickStartSpark.py --master=local[*] --deploy-mode=client

echo "ALL SYSTEMS STARTED"
echo "... DONE bootstrap.sh"


/bin/bash
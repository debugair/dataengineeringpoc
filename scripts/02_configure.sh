echo "START 02_configure.sh ..."


################################################
#   SET SSH
#################################################
echo "SETUP SSH"
ssh-keygen -t rsa -P '' -f ~/.ssh/id_rsa
cat ~/.ssh/id_rsa.pub >> ~/.ssh/authorized_keys
chmod 0600 ~/.ssh/authorized_keys
cp /docker_build/ssh/config /root/.ssh/config


#################################################
#   CONFIGURE THE TOOLS
#################################################
echo "CONFIGURE TOOLS"
cp /docker_build/hadoop/* /opt/hadoop/etc/hadoop/
cp /docker_build/spark/* /opt/spark/conf/
cp /docker_build/zeppelin/*.xml /opt/zeppelin/conf/
cp /docker_build/zeppelin/*.sh /opt/zeppelin/conf/
cp /docker_build/scripts/.bashrc /root/.bashrc
cp /docker_build/scripts/bootstrap.sh /etc/bootstrap.sh

chmod +x /opt/hadoop/etc/hadoop/*-env.sh
chmod +x /opt/spark/conf/*-env.sh
chmod +x /opt/zeppelin/conf/*-env.sh

mkdir -p /opt/zeppelin/notebooks
chown root:root -R /opt/zeppelin/notebooks
cp /docker_build/zeppelin/notebooks/* /opt/zeppelin/notebooks/

mkdir -p /opt/hadoop_tmp
mkdir -p /opt/hadoop_tmp/hdfs
mkdir -p /opt/hadoop_tmp/hdfs/namenode
mkdir -p /opt/hadoop_tmp/hdfs/datanode
mkdir -p /opt/hadoop/logs

chown root:root -R /opt/hadoop_tmp
chown root:root -R /opt/hadoop_tmp/hdfs
chown root:root -R /opt/hadoop_tmp/hdfs/namenode
chown root:root -R /opt/hadoop_tmp/hdfs/datanode
chown root:root -R /opt/hadoop/logs

mkdir -p /opt/airflow/dags
chown root:root -R /opt/airflow/dags
mkdir -p /opt/download
chown root:root -R /opt/download



#################################################
#   SET ENV VARS
#################################################
export JAVA_HOME=$(readlink -f /usr/bin/java | sed "s:jre/bin/java::")
#HADOOP
export HADOOP_INSTALL=/opt/hadoop
export PATH=$PATH:$HADOOP_INSTALL/bin
export PATH=$PATH:$HADOOP_INSTALL/sbin
export HADOOP_MAPRED_HOME=$HADOOP_INSTALL
export HADOOP_COMMON_HOME=$HADOOP_INSTALL
export HADOOP_HDFS_HOME=$HADOOP_INSTALL
export HADOOP_HOME=$HADOOP_INSTALL
export HADOOP_CONF_DIR=$HADOOP_HOME/etc/hadoop
export LD_LIBRARY_PATH=$HADOOP_HOME/lib/native:$LD_LIBRARY_PATH
export HADOOP_HOME_WARN_SUPPRESS=1
export HADOOP_ROOT_LOGGER="WARN,DRFA"
export HADOOP_OPTS="$HADOOP_OPTS -Djava.library.path=$HADOOP_HOME/lib/native"
export HADOOP_OPTS="$HADOOP_OPTS -XX:-PrintWarnings -Djava.net.preferIPv4Stack=true"
#SPARK
export SPARK_HOME=/opt/spark
export PATH=$PATH:$SPARK_HOME/bin
export SPARK_DIST_CLASSPATH=$(hadoop classpath)
#ZEPPELIN
export PATH=$PATH:/opt/zeppelin/bin
#AIRFLOW
export AIRFLOW_HOME=/opt/airflow

#################################################
#   CONFIGURE THE TOOLS
#################################################
echo "FORMAT AND START HDFS"
$HADOOP_HOME/etc/hadoop/hadoop-env.sh
$HADOOP_HOME/bin/hdfs namenode -format
service ssh start
sed s/HOSTNAME/$HOSTNAME/ /opt/hadoop/etc/hadoop/core-site.xml.template > /opt/hadoop/etc/hadoop/core-site.xml
$HADOOP_HOME/sbin/start-all.sh

$HADOOP_HOME/bin/hdfs dfs -mkdir -p /user/
$HADOOP_HOME/bin/hdfs dfs -mkdir -p /user/root/
$HADOOP_HOME/bin/hdfs dfs -mkdir /user/spark
$HADOOP_HOME/bin/hdfs dfs -mkdir /user/spark/share
$HADOOP_HOME/bin/hdfs dfs -mkdir /user/spark/share/lib
$HADOOP_HOME/bin/hdfs dfs -put /opt/spark/jars/*.jar /user/spark/share/lib/
$HADOOP_HOME/bin/hdfs dfs -mkdir /data
$HADOOP_HOME/bin/hdfs dfs -mkdir /data/Lei
$HADOOP_HOME/bin/hdfs dfs -mkdir /data/Lei/Raw
$HADOOP_HOME/bin/hdfs dfs -mkdir /data/Hdfs
$HADOOP_HOME/bin/hdfs dfs -mkdir /data/Hdfs/Dumps

airflow initdb
nohup /usr/local/bin/airflow scheduler &>/dev/null &
nohup /usr/local/bin/airflow webserver -p 8082 --pid /opt/airflow/webserver.pid &>/dev/null &
nohup /usr/local/bin/airflow scheduler &>/dev/null &

cp -r docker_build/airflow/dags/* /opt/airflow/dags/
cp /docker_build/airflow/airflow.cfg /opt/airflow/airflow.cfg

airflow delete_dag -y example_bash_operator
airflow delete_dag -y example_branch_dop_operator_v3
airflow delete_dag -y example_branch_operator
airflow delete_dag -y example_complex
airflow delete_dag -y example_external_task_marker_child
airflow delete_dag -y example_external_task_marker_parent
airflow delete_dag -y example_http_operator
airflow delete_dag -y example_passing_params_via_test_command
airflow delete_dag -y example_pig_operator
airflow delete_dag -y example_python_operator
airflow delete_dag -y example_short_circuit_operator
airflow delete_dag -y example_nested_branch_dag
airflow delete_dag -y example_skip_dag
airflow delete_dag -y example_subdag_operator
airflow delete_dag -y example_trigger_controller_dag
airflow delete_dag -y example_trigger_target_dag
airflow delete_dag -y example_xcom
airflow delete_dag -y latest_only
airflow delete_dag -y latest_only_with_trigger
airflow delete_dag -y test_utils
airflow delete_dag -y tutorial

echo "... DONE 02_configure.sh"
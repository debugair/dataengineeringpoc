echo "START 01_prepare.sh ..."


#################################################
#   INSTALL ADDITIONAL PACKAGES
#################################################
echo "APT-GET ADDITIONAL PACKAGES"
apt-get update
apt-get -y install software-properties-common
add-apt-repository ppa:deadsnakes/ppa
apt-get update
apt-get upgrade
#apt-get -y install redis-server
apt-get -y install openssh-client
apt-get -y install openssh-server
apt-get -y install openjdk-8-jdk
apt-get -y install wget
apt-get -y install tar
apt-get -y install python
apt-get -y install python-pip
apt-get -y install python3.7
apt-get -y install python3-pip
apt-get -y install netcat



#################################################
#   INSTALL HADOOP
#################################################
echo "SETUP HADOOP"
#cd && wget https://downloads.apache.org/hadoop/common/hadoop-3.2.1/hadoop-3.2.1.tar.gz
cd && wget http://apache.mirror.anlx.net/hadoop/common/hadoop-3.2.1/hadoop-3.2.1.tar.gz
tar -xvf hadoop-3.2.1.tar.gz -C /opt
rm hadoop-3.2.1.tar.gz && cd /opt
mv hadoop-3.2.1 hadoop
chown root:root /opt/hadoop




#################################################
#   INSTALL SPARK
#################################################
echo "SETUP SPARK"
#cd && wget https://downloads.apache.org/spark/spark-2.4.5/spark-2.4.5-bin-without-hadoop.tgz
#cd && wget http://apache.mirror.anlx.net/spark/spark-2.4.5/spark-2.4.5-bin-without-hadoop.tgz
cd && wget http://apache.mirror.anlx.net/spark/spark-2.4.5/spark-2.4.5-bin-without-hadoop-scala-2.12.tgz
tar -xvf spark-2.4.5-bin-without-hadoop-scala-2.12.tgz -C /opt/
rm spark-2.4.5-bin-without-hadoop-scala-2.12.tgz && cd /opt
mv spark-2.4.5-bin-without-hadoop-scala-2.12 spark
chown root:root /opt/spark


#################################################
#   INSTALL ZEPPELIN
#################################################
echo "SETUP ZEPPELIN"
#cd && wget https://downloads.apache.org/zeppelin/zeppelin-0.9.0-preview1/zeppelin-0.9.0-preview1-bin-all.tgz
cd && wget http://apache.mirror.anlx.net/zeppelin/zeppelin-0.9.0-preview1/zeppelin-0.9.0-preview1-bin-all.tgz
tar -xvf zeppelin-0.9.0-preview1-bin-all.tgz -C /opt
rm zeppelin-0.9.0-preview1-bin-all.tgz && cd /opt
mv zeppelin-0.9.0-preview1-bin-all zeppelin
chown root:root /opt/zeppelin


#################################################
#   INSTALL AIRFLOW
#################################################
echo "SETUP AIRFLOW"
export AIRFLOW_HOME=/opt/airflow
pip3 install apache-airflow
pip3 install pathlib
pip3 install pyspark

echo "... DONE 01_prepare.sh"
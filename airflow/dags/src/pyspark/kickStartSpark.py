import sys
import pyspark
from pyspark.context import SparkContext
from pyspark.sql.session import SparkSession


if not "sc" in globals():
    sc = pyspark.SparkContext()
spark = SparkSession(sc)


print("Python version")
print (sys.version)
print("Version info.")
print (sys.version_info)
print("Spark version")
print(spark.version)

import os
import pyspark
from datetime import datetime
from functools import reduce
from pyspark.context import SparkContext
from pyspark.sql.session import SparkSession
from pyspark.sql.types import IntegerType
from pyspark.sql.types import TimestampType
from pyspark.sql.functions import from_unixtime, unix_timestamp, lit


# CONSTANCES
__hdfsDumps = "/data/Hdfs/Dumps/"
__hdfsDumpsAgg = "/data/Hdfs/DumpsAgg/"


if not "sc" in globals():
    sc = pyspark.SparkContext()
spark = SparkSession(sc)


def main():
    dumpFiles, df = getDumpsAggregated()
    if not df is None:
        writeDumpsAggregated(df)
    deleteAggregatedFiles(dumpFiles)

   
def getHdfsFs():
    hadoop = sc._jvm.org.apache.hadoop
    conf = hadoop.conf.Configuration()
    fs = hadoop.fs.FileSystem.get(conf)
    return fs


def getHdfsPath(path: str):
    hadoop = sc._jvm.org.apache.hadoop
    hdfsPath = hadoop.fs.Path(path)
    return hdfsPath
    
    
def checkHdfsPathExists(path: str):
    fs = getHdfsFs()
    hdfsPath = getHdfsPath(path)
    pathExists = fs.exists(hdfsPath)
    return pathExists


def deleteHdfsPath(path: str):
    if checkHdfsPathExists(path) == False:
        return False
    
    fs = getHdfsFs()
    hdfsPath = getHdfsPath(path)
    result = fs.delete(hdfsPath, True)
    return result

    
def getHdfsPathList(path: str):
    pathList = []
    if checkHdfsPathExists(path) == False:
        return pathList
    
    fs = getHdfsFs()
    hdfsPath = getHdfsPath(path)
    
    for entry in fs.listStatus(hdfsPath):
        pathList.append(entry.getPath())
    
    return pathList
    
    
def renameDataFrameColumns(dataFrame, newColumns):
    oldColumns = dataFrame.schema.names
    return reduce(lambda dataFrame, idx: dataFrame.withColumnRenamed(oldColumns[idx], newColumns[idx]), range(len(oldColumns)), dataFrame)

    
def readDumpFile(path:str):
    dumpFileName = os.path.basename(path)
    nameParts = dumpFileName.split("_")
    timePart = nameParts[0] + " " + nameParts[1]
    dumpDate = datetime.strptime(timePart, "%Y-%m-%d %H-%M-%S")
    df = spark.read.format("csv") \
                    .option("sep", ",") \
                    .option("inferSchema", "true") \
                    .option("header", "true") \
                    .option("encoding", "UTF-8") \
                    .load(path)
    newColumns = ["Path", "ParentPath", "Size"]
    df = renameDataFrameColumns(df, newColumns)
    df = df.withColumn("DumpDate", lit(dumpDate))
    df = df.withColumn("DumpFile", lit(path))
    
    return df


def getDumpsAggregated():
    dumpFiles = getHdfsPathList(__hdfsDumps)
    dfAllDumps = None
    for dumpFile in dumpFiles:
        if str(dumpFile).endswith(".csv"):
            df = readDumpFile(str(dumpFile))
            if dfAllDumps:
                dfAllDumps = dfAllDumps.union(df)
            else:
                dfAllDumps = df
    return dumpFiles, dfAllDumps


def writeDumpsAggregated(df):
    df.write.mode("append") \
            .parquet(__hdfsDumpsAgg)


def deleteAggregatedFiles(dumpFiles):
    for dumpFile in dumpFiles:
        dumpFileName = os.path.basename(str(dumpFile))
        dumpFilePath = os.path.join(__hdfsDumps, dumpFileName)
        deleteHdfsPath(dumpFilePath)


if __name__ == "__main__":
    main()
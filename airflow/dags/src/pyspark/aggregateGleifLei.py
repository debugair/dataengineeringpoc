import pyspark
from pyspark.context import SparkContext
from pyspark.sql.session import SparkSession
import os
from datetime import datetime
from pyspark.sql.functions import from_unixtime, unix_timestamp, lit


# CONSTANCES
__hdfsLei = "/data/Lei/Raw/"
__hdfsLeiAgg = "/data/Lei/CH/"
__queryCH = """
            SELECT LEI, 
                    `Entity.LegalName` AS EntityName,
                    `Entity.LegalAddress.FirstAddressLine` AS Address1,
                    `Entity.LegalAddress.AdditionalAddressLine.1` AS Address2,
                    `Entity.LegalAddress.City` AS City,
                    `Entity.LegalAddress.Region` AS Region,
                    `Entity.LegalAddress.Country` AS Country,
                    `Entity.LegalAddress.PostalCode` AS Zip,
                    `Entity.LegalJurisdiction` AS LegalJurisdiction,
                    `Entity.EntityCategory` AS EntityCategory,
                    `Entity.LegalForm.EntityLegalFormCode` AS LegalForm,
                    `Entity.LegalForm.OtherLegalForm` AS OtherLegalForm,
                    `Entity.EntityStatus` AS Status,
                    FileDate,
                    FileName
            FROM GleifLei
            WHERE
                `Entity.LegalAddress.Country` = 'CH'
        """


if not "sc" in globals():
    sc = pyspark.SparkContext()
spark = SparkSession(sc)


def main():
    processGleifLeiFiles()


def getHdfsFs():
    hadoop = sc._jvm.org.apache.hadoop
    conf = hadoop.conf.Configuration()
    fs = hadoop.fs.FileSystem.get(conf)
    return fs


def getHdfsPath(path: str):
    hadoop = sc._jvm.org.apache.hadoop
    hdfsPath = hadoop.fs.Path(path)
    return hdfsPath
    
    
def checkHdfsPathExists(path: str):
    fs = getHdfsFs()
    hdfsPath = getHdfsPath(path)
    pathExists = fs.exists(hdfsPath)
    return pathExists


def deleteHdfsPath(path: str):
    if checkHdfsPathExists(path) == False:
        return False
    
    fs = getHdfsFs()
    hdfsPath = getHdfsPath(path)
    result = fs.delete(hdfsPath, True)
    return result

    
def getHdfsPathList(path: str):
    pathList = []
    if checkHdfsPathExists(path) == False:
        return pathList
    
    fs = getHdfsFs()
    hdfsPath = getHdfsPath(path)
    
    for entry in fs.listStatus(hdfsPath):
        pathList.append(entry.getPath())
    
    return pathList


def processGleifLeiFiles():
    leiFiles = getHdfsPathList(__hdfsLei)
    for leiFile in leiFiles:
        if str(leiFile).endswith(".csv"):
            df = readGleifLeiFile(str(leiFile))
            df.write.mode("append") \
                    .parquet(__hdfsLeiAgg)


def deleteGleifLeiFile(path:str):
    leiFileName = os.path.basename(str(path))
    leiFilePath = os.path.join(__hdfsLei, leiFileName)
    deleteHdfsPath(leiFilePath)


def readGleifLeiFile(path:str):
    fileName = os.path.basename(path)
    nameParts = fileName.split("-")
    timePart = nameParts[0] + " " + nameParts[1]
    dataDate = datetime.strptime(timePart, "%Y%m%d %H%M")
    dfGleifLei = spark.read.format("csv") \
                    .option("sep", ",") \
                    .option("inferSchema", "false") \
                    .option("header", "true") \
                    .option("encoding", "UTF-8") \
                    .load(path)
    dfGleifLei = dfGleifLei.withColumn("FileDate", lit(dataDate))
    dfGleifLei = dfGleifLei.withColumn("FileName", lit(path))
    dfGleifLei.createOrReplaceTempView("GleifLei")
    dfGleifLeiCH = spark.sql(__queryCH)
    return dfGleifLeiCH


if __name__ == "__main__":
    main()
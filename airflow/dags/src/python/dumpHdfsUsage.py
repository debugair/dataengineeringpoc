import subprocess
from datetime import datetime
from pathlib import Path
import os

# CONSTANCES
__hdfsCmd = "/opt/hadoop/bin/hdfs"
__hdfsFolder = "/"
__csvHeader = "name,parent,value\r\n"
__localFolder = "/opt/download/"
__hdfsFolder = "/data/Hdfs/Dumps/"


def main():
    writeDumpToHdfs(__localFolder, __hdfsFolder)


def writeDumpToHdfs(local:str, hdfs:str):
    csv = getDumpCsv()
    now = datetime.now()
    datePart =  now.strftime("%Y-%m-%d_%H-%M-%S")
    fileName = datePart + "_hdfs-dump.csv"
    filePathLocal = os.path.join(local, fileName)
    with open(filePathLocal, "w") as text_file:
        text_file.write(csv)
    cmd = [__hdfsCmd, "dfs", "-put", filePathLocal, hdfs]
    run_cmd(cmd)
    os.remove(filePathLocal)


def getDumpCsv() -> str:
    entries = getDump()
    content = __csvHeader
    for entry in entries:
        content += entry[0] + "," + entry[1] +"," + str(entry[2]) + "\r\n"
    return content


def getDump() -> list:
    cmd = [__hdfsCmd, "dfs", "-ls", "-R", "/"]
    (ret, out, err) = run_cmd(cmd)
    print(ret, out, err)
    lines = out.decode("utf-8").split("\n")
    hdfsEntries = []
    for line in lines:
        cols = line.split(" ")
        cols = [x for x in cols if x]
        if len(cols) == 8:
            path = cols[7]
            parent = getParent(path)
            size = cols[4]
            hdfsEntries.append((path,parent,size))
    return hdfsEntries


def getParent(path:str) -> str:
    currPath = Path(path).resolve()
    parentPath = str(currPath.parent)
    return parentPath


def run_cmd(args_list):
        """
        run linux commands
        """
        # import subprocess
        print("Running system command: {0}".format(" ".join(args_list)))
        proc = subprocess.Popen(args_list, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        s_output, s_err = proc.communicate()
        s_return =  proc.returncode
        return s_return, s_output, s_err


if __name__ == "__main__":
    main()
import argparse
import urllib.request
import json
import subprocess
import requests
import os
import errno
import zipfile
from urllib.parse import urlparse
from datetime import datetime
from datetime import timedelta


# CONSTANCES
__gleifLeiUrlPattern = "https://leidata-preview.gleif.org/api/v2/golden-copies/publishes?page=1&per_page=%(days)s"
__defaultDayHistory = 30
__defaultDataFormat="csv"
__localFolder = "/opt/download/"
__hdfsFolder = "/data/Lei/Raw/"
__hdfsCmd = "/opt/hadoop/bin/hdfs"
__args = None


if os.name == "nt":
    __localFolder = "D:\\Lei\\Raw\\"


def main():
    __args = initParser()
    __hdfsCmd = __args.hdfsCmd
    fileIndex = getGleifLeiFileIndex(__args.days, __args.format)
    downloadFiles(fileIndex, __args.local, __args.hdfs, __args.days)
   


def initParser():
    parser = argparse.ArgumentParser(description="Downloads daily LEI data files from gleif.org.")
    parser.add_argument("--days", type=int, default=__defaultDayHistory, required=False,
                        help="indicates how many days of historical LEI data should be downloaded.")
    parser.add_argument("--format", type=str, choices= ["csv", "json", "xml"], 
                        default=__defaultDataFormat, required=False,
                        help="defines the format in which the data shall be downloaded.")
    parser.add_argument("--local", type=str, default=__localFolder, required=False,
                        help="where to store the downloaded files.")
    parser.add_argument("--hdfs", type=str, default=__hdfsFolder, required=False,
                        help="where to put the downloaded files on hdfs.")
    parser.add_argument("--hdfsCmd", type=str, default=__hdfsCmd, required=False,
                        help="where to put the downloaded files on hdfs.")
    args = parser.parse_args()
    return args
    

def getGleifLeiFileIndex(dayHistory:int, format:str) -> dict:
    urlParts = {"days": (dayHistory + 1) * 3}
    url = __gleifLeiUrlPattern % urlParts
    with urllib.request.urlopen(url) as request:
        data = json.loads(request.read().decode())

    fileIndex = {}
    for entry in data["data"]:
        fileDate = datetime.strptime(entry["publish_date"], "%Y-%m-%d %H:%M:%S")
        fileUrl = entry["lei2"]["full_file"][format]["url"]
        fileIndex[fileDate] = fileUrl
    return fileIndex


def downloadFiles(fileIndex, local:str, hdfs:str, dayHistory:int):
    for key in sorted(fileIndex.keys()):
        delta = datetime.now() - key
        if delta.days <= dayHistory:
            (localZip, localData, hdfsData) = getFileNames(fileIndex[key], local, hdfs)
            if fileExistsInHdfs(localData, hdfsData) == False:
                downloadFile(fileIndex[key], localZip)
                unzipFile(localZip, local)
                uploadFile(localData, hdfsData)


def getFileNames(url:str, local:str, hdfs:str) -> (str, str, str):
    fileUrl = urlparse(url)
    fileNameZip = os.path.basename(fileUrl.path)
    fileNameData = fileNameZip.replace(".zip", "")
    fullLocalZipPath = os.path.join(local, fileNameZip)
    fullLocalDataPath = os.path.join(local, fileNameData)
    fullHdfsDataPath = os.path.join(hdfs, fileNameData)
    return fullLocalZipPath, fullLocalDataPath, fullHdfsDataPath


def downloadFile(url:str, local:str):
    print("download file '" + local + "'")
    foundFile = requests.get(url)
    open(local, "wb").write(foundFile.content)


def unzipFile(zipFile:str, destination:str, removeZip:bool=True) -> str:
    print("unzip file '" + zipFile + "'")
    with zipfile.ZipFile(zipFile, "r") as zipHandle:
        zipHandle.extractall(destination)
    if(removeZip):
        os.remove(zipFile)


def uploadFile(file:str, hdfs:str, removeFile:bool=True):
    if os.path.exists(file) == False:
        raise FileNotFoundError(
            errno.ENOENT, os.strerror(errno.ENOENT), file)
    
    print("upload file '" + file + "'")
    cmd = [__hdfsCmd, "dfs", "-put", file, hdfs]
    (ret, out, err)= run_cmd(cmd)
    print(ret)
    print(out)
    print(err)
    if(removeFile):
        os.remove(file)


def fileExistsInHdfs(local:str, hdfs:str) -> bool:
    # https://community.cloudera.com/t5/Community-Articles/Interacting-with-Hadoop-HDFS-using-Python-codes/ta-p/245163
    if os.name == "nt":
        return False
    
    cmd = [__hdfsCmd, "dfs", "-test", "-e", hdfs]
    (ret, out, err) = run_cmd(cmd)
    print(ret)
    print(out)
    print(err)
    if(ret):
        return False
    else:
        return True


def run_cmd(args_list):
        """
        run linux commands
        """
        # import subprocess
        print("Running system command: {0}".format(" ".join(args_list)))
        proc = subprocess.Popen(args_list, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        s_output, s_err = proc.communicate()
        s_return =  proc.returncode
        return s_return, s_output, s_err 


if __name__ == "__main__":
    main()
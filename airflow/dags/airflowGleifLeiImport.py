from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from datetime import datetime, timedelta
import os


print(__name__)


srcDir = os.path.dirname(os.path.realpath(__file__))
sparkSubmit = '/opt/spark/bin/spark-submit'


## Define the DAG object
default_args = {
    'owner': 'root',
    'depends_on_past': True,
    'start_date': datetime(2020, 5, 9),
    'retries': 3,
    'retry_delay': timedelta(minutes=1),
}
dag = DAG('downloadGleifLeiData', default_args=default_args, schedule_interval="0 * * * *", is_paused_upon_creation=False, max_active_runs=1)


cmdPattern = "python3 %(src)s/src/python/gleifLeiDownload.py --days %(days)s --format %(format)s --local %(local)s --hdfs %(hdfs)s --hdfsCmd %(hdfsCmd)s"
days = 5
fileFormat = "csv"
local = "/opt/download/"
hdfs = "/data/Lei/Raw/"
hdfsCmd = "/opt/hadoop/bin/hdfs"

params = {
            "days": days,
            "format": fileFormat,
            "local": local,
            "hdfs": hdfs,
            "hdfsCmd": hdfsCmd,
            "src": srcDir
        }
cmd = cmdPattern % params

downloadLeiData= BashOperator(
    task_id='download-lei-data',
    bash_command=cmd,
    dag=dag)


aggregateGleifLeiData = BashOperator(
    task_id='aggregate-lei-data',
    bash_command=sparkSubmit + " " + srcDir + "/src/pyspark/aggregateGleifLei.py --master=local[*] --deploy-mode=client",
    dag=dag)


aggregateGleifLeiData.set_upstream(downloadLeiData)
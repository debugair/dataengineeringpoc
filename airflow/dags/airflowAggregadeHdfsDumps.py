from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from datetime import datetime, timedelta
import os


print(__name__)


srcDir = os.path.dirname(os.path.realpath(__file__))
sparkSubmit = '/opt/spark/bin/spark-submit'


## Define the DAG object
default_args = {
    'owner': 'root',
    'depends_on_past': False,
    'start_date': datetime(2020, 5, 9),
    'retries': 3,
    'retry_delay': timedelta(minutes=1),
}
dag = DAG('aggregateHdfsDumps', default_args=default_args, schedule_interval="*/3 * * * *", is_paused_upon_creation=False, max_active_runs=1)


aggregateHdfsDumpData = BashOperator(
    task_id='aggregate-hdfs-data',
    bash_command=sparkSubmit + " " + srcDir + "/src/pyspark/aggregateHdfsUsages.py --master=local[*] --deploy-mode=client",
    dag=dag)